import { Dispatch } from "react";
import { AxiosResponse } from "axios";
import { CallHttpTodoAction, HttpTodoActionType } from "./HttpActionBuilder";


/********************************************
 *       --- Http Todo REST methods ---
 ********************************************/
// --- Http DELETE Todo ---
export const HttpActionDeleteTodoAndDispatchState = async (
  dispatch: Dispatch<any>,
  todoId: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_DELETE_TODO, todoId, '', actionResponseSucces, actionResponseError, dispatch)
};

// --- Http POST Create New Todo ---
export const HttpActionCreateNewTodoAndDispatchState = async (
  dispatch: Dispatch<any>,
  todoText: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_CREATE_NEW_TODO, '', todoText, actionResponseSucces, actionResponseError, dispatch)
};

// --- Http GET GetTodos ---
export const HttpActionGetTodosAndDispatchState = async (
  dispatch: Dispatch<any>,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_GET_TODOS, '', '', actionResponseSucces, actionResponseError, dispatch)
};

// --- Http POST Update Todo ---
export const HttpActionUpdateTodoAndDispatchState = async (
  dispatch: Dispatch<any>,
  todoId: string,
  todoText: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_UPDATE_TODO, todoId, todoText, actionResponseSucces, actionResponseError, dispatch)
};

// --- Http POST Complete Todo ---
export const HttpActionCompleteTodoAndDispatchState = async (
  dispatch: Dispatch<any>,
  todoId: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_COMPLETE_TODO, todoId, '', actionResponseSucces, actionResponseError, dispatch)
};

// --- Http POST InComplete Todo ---
export const HttpActionInCompleteTodoAndDispatchState = async (
  dispatch: Dispatch<any>,
  todoId: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void
) => {
    CallHttpTodoAction(HttpTodoActionType.HTTP_INCOMPLETE_TODO, todoId, '', actionResponseSucces, actionResponseError, dispatch)
};
