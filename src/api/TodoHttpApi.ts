import axios from "axios";
import {
  BASE_URL,
  REQUEST_PARAM_TODOS,
  REQUEST_PARAM_COMPLETED,
  REQUEST_PARAM_COMPLETE,
  REQUEST_PARAM_INCOMPLETE,
} from "./http-constants";

const axiosInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 15000,
});

export const TodoHttpApi = {

  // --- GET /todos ---
  getTodos: () => {
    return axiosInstance.request({
      method: "GET",
      url: `/${REQUEST_PARAM_TODOS}`,
    });
  },

  // --- POST /todos ---
  createTodo: (todoText: string) => {
    return axiosInstance.request({
      method: "POST",
      url: `/${REQUEST_PARAM_TODOS}`,
      data: {
        text: todoText,
      },
    });
  },

  // --- GET /todos/completed ---
  getCompletedTodos: () => {
    return axiosInstance.request({
      method: "GET",
      url: `/${REQUEST_PARAM_TODOS}/${REQUEST_PARAM_COMPLETED}`,
    });
  },

  // --- POST /todos/{id} ---
  updateTodo: (todoId: string, todoText: string) => {
    return axiosInstance.request({
      method: "POST",
      url: `/${REQUEST_PARAM_TODOS}/${todoId}`,
      data: {
        text: todoText,
      },
    });
  },

  // --- DELETE /todos/{id} ---
  deleteTodo: (todoId: string) => {
    return axiosInstance.request({
      method: "DELETE",
      url: `/${REQUEST_PARAM_TODOS}/${todoId}`,
    });
  },

  // --- POST /todos/{id}/complete ---
  completeTodo: (todoId: string) => {
    return axiosInstance.request({
      method: "POST",
      url: `/${REQUEST_PARAM_TODOS}/${todoId}/${REQUEST_PARAM_COMPLETE}`,
    });
  },

  // --- POST /todos/{id}/incomplete ---
  incompleteTodo: (todoId: string) => {
    return axiosInstance.request({
      method: "POST",
      url: `/${REQUEST_PARAM_TODOS}/${todoId}/${REQUEST_PARAM_INCOMPLETE}`,
    });
  }
  
};
