import { HTTP_422_ERROR, HTTP_404_ERROR } from "./http-error-constans";

export const getHttpErrorMessage = (httpError: any): string => {
  let errorMessage = "null";

  if (httpError.response) {
    switch (httpError.response.status) {
      case HTTP_404_ERROR:
        errorMessage = httpError.response.data;
        break;

      case HTTP_422_ERROR:
        errorMessage = httpError.response.data;
        break;

      default:
        errorMessage = "Server error";
        break;
    }
  } else if (httpError.request) {
    errorMessage = "Network Error";
  } else {
    errorMessage = httpError.message;
  }

  return errorMessage;
};
