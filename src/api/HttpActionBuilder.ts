import { Dispatch } from "react";
import { AxiosResponse } from "axios";
import { TodoHttpApi } from "./TodoHttpApi";
import { dispatchActionAppStateObject } from "../redux-store/DispatcherActions";
import { AppStateObjectReducerEnum } from "../redux-store/AppStateObjectReducer";
import { AppStateObject } from "../model/AppStateObject";


export enum HttpTodoActionType {
  HTTP_DELETE_TODO = 'HTTP_DELETE_TODO',
  HTTP_CREATE_NEW_TODO = 'HTTP_CREATE_NEW_TODO',
  HTTP_GET_TODOS = 'HTTP_GET_TODOS',
  HTTP_UPDATE_TODO = 'HTTP_UPDATE_TODO',
  HTTP_COMPLETE_TODO = 'HTTP_COMPLETE_TODO',
  HTTP_INCOMPLETE_TODO = 'HTTP_INCOMPLETE_TODO'
} 

export const CallHttpTodoAction = (
  httpActionType: HttpTodoActionType,
  todoId: string,
  todoText: string,
  actionResponseSucces: (response: AxiosResponse<unknown, any>) => void,
  actionResponseError: (error: any) => void,
  dispatch: Dispatch<any>
): void => {
  switch (httpActionType) {
    case HttpTodoActionType.HTTP_DELETE_TODO:
      TodoHttpApi.deleteTodo(todoId)
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });
      break;

    case HttpTodoActionType.HTTP_CREATE_NEW_TODO:
      TodoHttpApi.createTodo(todoText)
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });
      break;

    case HttpTodoActionType.HTTP_GET_TODOS:
      TodoHttpApi.getTodos()
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });

      break;

    case HttpTodoActionType.HTTP_UPDATE_TODO:
      TodoHttpApi.updateTodo(todoId, todoText)
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });
      break;

    case HttpTodoActionType.HTTP_COMPLETE_TODO:
      TodoHttpApi.completeTodo(todoId)
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });
      break;

    case HttpTodoActionType.HTTP_INCOMPLETE_TODO:
      TodoHttpApi.incompleteTodo(todoId)
        .then(function (response) {
          actionResponseSucces(response);
        })
        .catch(function (error: any) {
          actionResponseError(error);
        });
      break;

    default:
      break;
  }

  // --- dispatch state in all Http request ---
  dispatchActionAppStateObject(
    AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT,
    dispatch, { isLoading: true, isError: false, errorMessage: null } as AppStateObject
  );

};
