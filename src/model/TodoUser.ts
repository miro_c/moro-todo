import { Todo } from "./Todo";

export interface TodoUser {
  user_id: string | undefined;
  todos: Array<Todo> | undefined
}
