
export interface AppStateObject {
  isLoading: boolean;
  isError: boolean;
  errorMessage: string | null;
}


