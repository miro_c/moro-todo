
export enum TodoActionState {
  ACTION_ALL = "ACTION_ALL",
  ACTION_ACTIVE = "ACTION_ACTIVE",
  ACTION_COMPLETED = "ACTION_COMPLETED"
}

export interface TodoState {
  todo_state: TodoActionState;
}


