import { BrowserRouter, Route, Switch } from "react-router-dom";
import { useEffect } from "react";
import PageSplashScreen from "./components/PageSplashComponent/PageSplashScreen";
import PageTodos from "./components/PageTodosComponent/PageTodos";
import PageError from "./components/PageErrorComponent/PageError";
import { useDispatch } from "react-redux";
import { GlobalStyle, AppMain } from "./App-style";
import { getTodoUserFromLocalStorage, getTodosStateFromLocalStorage, constant_param } from "./utils/LocalStorageUtil";
import { DispatchTodoState, dispatchActionAppStateObject, dispatchTodoActionState } from "./redux-store/DispatcherActions";
import { AppStateObjectReducerEnum } from "./redux-store/AppStateObjectReducer";
import { TodoStateEnum } from "./redux-store/TodoStateReducer";
import { TodoActionState } from "./model/TodoState";
import { TodoReducerActionEnum } from "./redux-store/TodoReducer";
import { AppStateObject } from "./model/AppStateObject";


const App = () => {
  const dispatch = useDispatch();
  const currentCachedUser = getTodoUserFromLocalStorage(constant_param.USER_KEY);
  const cachedTodosState = getTodosStateFromLocalStorage(constant_param.TODOS_STATE_KEY);

  useEffect(() => {

    // --- Init/Save TodoUserObject to AppState ---
    async function SetupTodoUserObjectAsync() {
      if (!currentCachedUser) {
        await DispatchTodoState(TodoReducerActionEnum.ACTION_CREATE_TODO_USER, dispatch, null);
      } else {
        await DispatchTodoState(TodoReducerActionEnum.ACTION_REFREESH_TODO_USER, dispatch, currentCachedUser);
      }
    }
    SetupTodoUserObjectAsync();
  }, []);

  useEffect(() => {
    // --- First time app running creating new AppStateObject ---
    async function SetupAppStateObjectAsync() {
      await dispatchActionAppStateObject(AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT, dispatch, {isLoading: true, isError: false, errorMessage: null} as AppStateObject);
    }
    SetupAppStateObjectAsync();
  }, []);

  useEffect(() => {
    // --- First time app running creating new TodoActionState ---
    async function SetupTodoStateAsync() { 
      if(!cachedTodosState){
        await dispatchTodoActionState(dispatch, cachedTodosState, TodoActionState.ACTION_ALL, TodoStateEnum.ACTION_TODO_STATE); 
      } else {
        await dispatchTodoActionState(dispatch, cachedTodosState, cachedTodosState.todo_state, TodoStateEnum.ACTION_TODO_STATE);  
      }
    }
    SetupTodoStateAsync();
  }, []);

  return (
    <AppMain>
      <GlobalStyle />
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={PageSplashScreen} />
          <Route exact path="/todos">
            <PageTodos />
          </Route>
          <Route component={PageError} />
        </Switch>
      </BrowserRouter>
    </AppMain>
  );
};

export default App;
