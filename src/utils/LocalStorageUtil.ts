import { TodoUser } from "../model/TodoUser";
import { Todo } from "../model/Todo";
import { TodoState, TodoActionState } from "../model/TodoState";

export const saveToLocalStorage = (key: string, uniqueId: string) => {
  window.localStorage.setItem(key, JSON.stringify(uniqueId));
};

export const getFromLocalStorage = (key: string): string | null => {
  const userId = window.localStorage.getItem(key);
  return userId;
};

export const saveTodoUserToLocalStorage = (
  key: string,
  user: TodoUser | null
) => {
  window.localStorage.setItem(key, JSON.stringify(user));
};

export const getTodoUserFromLocalStorage = (key: string): TodoUser | null => {
  let todoUserObject = null;

  const todoUserString = window.localStorage.getItem(key);
  if (todoUserString) {
    todoUserObject = JSON.parse(todoUserString) as TodoUser;
  }

  return todoUserObject;
};

export const saveTodosStateToLocalStorage = (
  key: string,
  todoState: TodoState | null
) => {
  window.localStorage.setItem(key, JSON.stringify(todoState));
};

export const getTodosStateFromLocalStorage = (key: string): TodoState | null => {
  let todosStateObject = null;

  const todosStateString = window.localStorage.getItem(key);
  if (todosStateString) {
    todosStateObject = JSON.parse(todosStateString) as TodoState;
  }

  return todosStateObject;
};

export const isDifferent = (
  userA: Todo[] | undefined,
  userB: Todo[] | undefined
): boolean => {
  return JSON.stringify(userA) !== JSON.stringify(userB);
};

export const constant_param = {
  LOCAL_STORAGE_KEY: "todoUserId",
  USER_KEY: "USER_KEY",
  LOCAL_STORAGE_VALUE: "",
  TODOS_STATE_KEY: "TODOS_STATE_KEY"
};
