import { TodoUser } from "../model/TodoUser";
import { Todo } from "../model/Todo";


export const getMarkCompletedFilteredTodos = (
  todoUser: TodoUser
): Todo[] | undefined => {

  const todosNumber = todoUser.todos?.length;
  let todosCompleted = 0;

  let filteredTodos;
  filteredTodos = todoUser.todos?.map((todo) => {
    if (todo.completed) {
      todosCompleted += 1;
    }
    todo.completed = true;
    return todo;
  });

  if (todosCompleted === todosNumber) { // --- All todos are selected(completed) ---
    filteredTodos = todoUser.todos?.map((todo) => { 
      todo.completed = false; 
      return todo;
    });
  }

  return filteredTodos;
};

export const getFilteredTodosByTodoId = (
  todoUser: TodoUser,
  todoId: string
): Todo[] | undefined => {
  const filteredTodos = todoUser.todos?.filter((todo) => todo.id !== todoId);
  return filteredTodos;
};

export const getMarkCompletedToggleTodos = (
  todoUser: TodoUser,
  todoId: string
): Todo[] | undefined => {
  const filteredTodos = todoUser.todos?.map((todo) => {
    if (todo.id === todoId) {
      todo.completed = !todo.completed;
    }
    return todo;
  });

  return filteredTodos;
};

export const constant_param = {
  LOCAL_STORAGE_KEY: "todoUserId",
  USER_KEY: "USER_KEY",
  LOCAL_STORAGE_VALUE: "",
};
