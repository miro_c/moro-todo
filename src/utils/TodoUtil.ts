import { v4 as uuidv4 } from "uuid";

export const getUniqueUserId = (): string => {
  const uuid = uuidv4().toString();
  const currentTimeInMilliseconds = Date.now().toString();
  const finalString = uuid.concat(currentTimeInMilliseconds);

  return finalString;
};





