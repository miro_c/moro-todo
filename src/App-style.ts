import styled, { createGlobalStyle } from "styled-components";
import { color }  from "./components/common-ui/css_constants"

export const GlobalStyle = createGlobalStyle`
    p, a,  th, td, tr, label, input, option, button {
        margin: 0;
        padding: 0;
        font-family: 'Roboto', sans-serif;
    }

   h1 {
      font-family: 'Anton', sans-serif;
    }

    *,
    *::before,
    *::after {
      box-sizing: border-box;
    }
`;

export const AppMain = styled.div`
  background: ${color.color_background};
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;
