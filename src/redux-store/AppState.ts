import { combineReducers } from "redux";
import { TodoReducer } from "./TodoReducer";
import { AppStateObjectReducer } from "./AppStateObjectReducer";
import { TodoStateReducer } from "./TodoStateReducer";

export const rootReducer = combineReducers({
  todo_user: TodoReducer,
  app_state_object: AppStateObjectReducer,
  todo_state: TodoStateReducer,
});

export type AppState = ReturnType<typeof rootReducer>;
