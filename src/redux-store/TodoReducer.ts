import { TodoUser } from "../model/TodoUser";
import { getUniqueUserId } from "../utils/TodoUtil";
import { saveToLocalStorage, constant_param } from "../utils/LocalStorageUtil";

export enum TodoReducerActionEnum {
  ACTION_CREATE_TODO_USER = "ACTION_CREATE_TODO_USER",
  ACTION_GET_TODOS = "ACTION_GET_TODOS",
  ACTION_CREATE_TODO = "ACTION_CREATE_TODO",
  ACTION_GET_COMPLETED_TODOS = "ACTION_GET_COMPLETED_TODOS",
  ACTION_UPDATE_TODO = "ACTION_UPDATE_TODO",
  ACTION_DELETE_TODO = "ACTION_DELETE_TODO",
  ACTION_COMPLETE_TODO = "ACTION_COMPLETE_TODO",
  ACTION_INCOMPLETE_TODO = "ACTION_INCOMPLETE_TODO",
  ACTION_TODOS_ERROR = "ACTION_TODOS_ERROR",
  ACTION_REFREESH_TODO_USER = "ACTION_REFREESH_TODO_USER",
}

export interface TodoAction {
  type: TodoReducerActionEnum;
  payload: TodoUser | null;
}

export const TodoReducer = (
  state: TodoUser | null = null,
  action: TodoAction
): TodoUser | null => {
  switch (action.type) {
    case TodoReducerActionEnum.ACTION_CREATE_TODO_USER: 
      let uniqueId = getUniqueUserId();
      saveToLocalStorage(constant_param.USER_KEY, uniqueId);
      return {
        user_id: uniqueId,
        todos: [],
      };

    case TodoReducerActionEnum.ACTION_GET_TODOS: 
      return {
        user_id: action.payload?.user_id,
        todos: action.payload?.todos,
      };

    case TodoReducerActionEnum.ACTION_CREATE_TODO: 
      return action.payload;

    case TodoReducerActionEnum.ACTION_GET_COMPLETED_TODOS:
      return action.payload;

    case TodoReducerActionEnum.ACTION_UPDATE_TODO:
      return action.payload;

    case TodoReducerActionEnum.ACTION_DELETE_TODO: 
      return {
        user_id: action.payload?.user_id,
        todos: action.payload?.todos,
      };

    case TodoReducerActionEnum.ACTION_COMPLETE_TODO: 
      return action.payload;

    case TodoReducerActionEnum.ACTION_INCOMPLETE_TODO: 
      return action.payload;

    case TodoReducerActionEnum.ACTION_TODOS_ERROR:
      return action.payload;

    case TodoReducerActionEnum.ACTION_REFREESH_TODO_USER: 
      return action.payload;

    default:
      return state;
  }
};
