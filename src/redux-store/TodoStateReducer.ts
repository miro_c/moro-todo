import { TodoState, TodoActionState } from "../model/TodoState";
import { constant_param, saveTodosStateToLocalStorage } from "../utils/LocalStorageUtil";

export enum TodoStateEnum {
  ACTION_TODO_STATE = "ACTION_TODO_STATE",
}

export interface TodoStateAction {
  type: TodoStateEnum;
  payload: TodoState | null;
}

export const TodoStateReducer = (
  state: TodoState | null = null,
  action: TodoStateAction
): TodoState | null => {
  switch (action.type) {
    case TodoStateEnum.ACTION_TODO_STATE:
      let actionState = null;
      if (!action.payload) {
        actionState = TodoActionState.ACTION_ALL;
      } else {
        actionState = action.payload.todo_state;
      }
      saveTodosStateToLocalStorage(
        constant_param.TODOS_STATE_KEY,
        action.payload
      );
      return {
        todo_state: actionState,
      };

    default:
      return state;
  }
};
