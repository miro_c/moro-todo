import { AppStateObject } from "../model/AppStateObject";

export enum AppStateObjectReducerEnum {
  ACTION_APP_STATE_OBJECT = "ACTION_APP_STATE_OBJECT",
}

export interface AppStateObjectAction {
  type: AppStateObjectReducerEnum;
  payload: AppStateObject | null;
}

export const AppStateObjectReducer = (
  state: AppStateObject | null = null,
  action: AppStateObjectAction
): AppStateObject | null => {
  switch (action.type) {
    case AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT:
         return action.payload;

    default:
      return state;
  }
};
