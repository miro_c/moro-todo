import { Dispatch } from "react";
import { TodoUser } from "../model/TodoUser";
import { AppStateObject } from "../model/AppStateObject";
import { TodoActionState, TodoState } from "../model/TodoState";
import { TodoStateEnum } from "../redux-store/TodoStateReducer";
import { TodoReducerActionEnum } from "../redux-store/TodoReducer";
import { AppStateObjectReducerEnum } from "../redux-store/AppStateObjectReducer";
import { getHttpErrorMessage } from "../api/HttpErrorHelper";
import { saveTodoUserToLocalStorage, constant_param } from "../utils/LocalStorageUtil";


// --- Dispatch Todo to AppState ---
export const DispatchTodoState = async (
  action_type: TodoReducerActionEnum,
  dispatch: Dispatch<any>,
  todoUser: TodoUser | null
) => {
  // --- set App State to normal state (isError = false, isLoading = false)
  DispatchAppStateToDefaultAction(dispatch);

  if (!todoUser) {
    dispatch({
      type: action_type,
      payload: {},
    });
  } else {
    dispatch({
      type: action_type,
      payload: {
        ...todoUser,
      },
    });

    // --- Save TodoUser with todos to LocalStorage ---
    saveTodoUserToLocalStorage(constant_param.USER_KEY, todoUser);
  }
};

// --- Save Error state to AppState ---
export const actionDispatchHttpError = (
  action_type: AppStateObjectReducerEnum,
  dispatch: Dispatch<any>,
  error: any,
  stateObject: AppStateObject | null
) => {
  const errorMsg = getHttpErrorMessage(error);
  if (errorMsg) {
    stateObject = {isLoading: false, isError: true, errorMessage: errorMsg} as AppStateObject;
    dispatchActionAppStateObject(action_type, dispatch, stateObject);
  }
};

// --- Dispatch AppStateObject (isLoading|isError|errorMessage) ---
export const dispatchActionAppStateObject = async (
  action_type: AppStateObjectReducerEnum,
  dispatch: Dispatch<any>,
  stateObject: AppStateObject | null
) => {
  dispatch({
    type: action_type,
    payload: {
      ...stateObject,
    },
  });
};

// ---  Dispatch TodoState (All|Active|Completed) ---
export const dispatchTodoActionState = async (
  dispatch: Dispatch<any>,
  todoState: TodoState | null,
  todoActionState: TodoActionState | null,
  action_type: TodoStateEnum
) => {
  dispatch({
    type: action_type,
    payload: {
      ...todoState,
      todo_state: todoActionState,
    },
  });
};

export const DispatchAppStateToDefaultAction = ( dispatch: Dispatch<any>) => {
  dispatchActionAppStateObject(
    AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT,
    dispatch,{ isLoading: false, isError: false, errorMessage: null }
  );
}
