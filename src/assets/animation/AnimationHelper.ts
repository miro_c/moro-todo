import { gsap } from "gsap";

export class AnimationHelper {
    static InitUiStateBeforeRender = (gsap: any, uiElements : Array<any>) => {
        gsap.set([uiElements[0]], {
            x: 15,
            opacity: 0,
        });

        gsap.set([uiElements[1]], {
            x: -15,
            opacity: 0,
        });
    };

    static SetupHorizontalAnimation(gsap: any, uiElements : Array<any>) {
        gsap.to([uiElements[0], uiElements[1]], {
            duration: 1.0,
            ease: "elastic(0.5, 0.5)",
            x: 0,
            opacity: 1,
            delay: 0.6
        });
    }

    static RunFinishAnimation(gsap: any, uiElements : Array<any>) {
        gsap.add('finish')
            .to([uiElements[0]], {
                duration: 0.5,
                ease: "elastic(1, 0.5)",
                x: 30,
                opacity: 0,
                delay: 0.5
            }, 'finish')
            .to([uiElements[1]], {
                duration: 0.5,
                ease: "elastic(1, 0.5)",
                x: -30,
                opacity: 0,
                delay: 0.5
            }, 'finish');
    }

    static RunStartAnimation(gsap: any, uiElements : Array<any>) {
        gsap.add('start')
            .to([uiElements[0]], {
                duration: 1.5,
                ease: "elastic(1, 0.5)",
                x: 0,
                opacity: 1,
                delay: 0.5
            }, 'start').to([uiElements[1]], {
                duration: 1.5,
                ease: "elastic(1, 0.5)",
                x: 0,
                opacity: 1,
                delay: 0.5
            }, 'start');
    }

}
