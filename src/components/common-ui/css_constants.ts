
export const color = {
   color_background: '#f5f5f5',
   color_light_grey_italic: '#d1d1d1',
   color_dark_grey: '#525151',
   color_light_grey: "#e5e5e5",
   color_material_purple: '#e2d7f4',
   color_black: '#000000',
   color_white:'#fff',
   color_purple: '#9a77cdff',
   color_grey: '#adadad'
}

export const text_size = {
   size_small: '0.5rem',
   size_medium: '1.5rem',
   size_medium_mobile: '1rem',
   size_medium_info: '0.8rem',
   size_big: '2.5rem',
   size_xxl: '5.5rem'
}
