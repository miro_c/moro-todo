import { FC, useRef, useEffect, useLayoutEffect } from "react";
import { SpinnerImg } from "./LoadingSpinner-style";
import { gsap } from "gsap";

const LoadingSpinner: FC = () => {
  const lineTop = useRef(null);
  const lineCenter = useRef(null);
  const lineBototm = useRef(null);
  const TimeLine = gsap.timeline({repeat: -1, repeatDelay: 0.02, yoyo: true});

  useLayoutEffect(() => {
    TimeLine.set([lineTop.current], { y: - 3, opacity: 0});
    TimeLine.set([lineCenter.current], { y: - 2, opacity: 0});
    TimeLine.set([lineBototm.current], { y: - 1, opacity: 0});

    return () => {
      TimeLine.clear(true);
    };
    
}, [TimeLine]);

  useEffect(() => {
    TimeLine
    .to([lineTop.current],{duration: 0.15, ease: "elastic(0.5, 5.5)", opacity: 1, y: 0,delay: 0.01})
    .to([lineCenter.current],{duration: 0.1, ease: "elastic(0.5, 5.5)", opacity: 1, y: 0,delay: 0.01})
    .to([lineBototm.current],{duration: 0.1, ease: "elastic(0.5, 5.5)", opacity: 1, y: 0,delay: 0.01});
  }, [TimeLine]);

  return (
    <>
      <SpinnerImg
        width="15.627171"
        height="6.740756"
        fill="none"
        version="1.1"
        viewBox="0 0 15.627171 6.7407561"
      >
        <style />
        <rect ref={lineTop}
          x=".0033663535"
          width="15.594007"
          height=".98645216"
          fill="#dccfee"
        />
        <rect ref={lineCenter}
          y="2.8771515"
          width="15.594007"
          height=".98645216"
          fill="#dccfee"
        />
        <rect ref={lineBototm}
          x=".033164419"
          y="5.7543039"
          width="15.594007"
          height=".98645216"
          fill="#dccfee"
        />
      </SpinnerImg>
    </>
  );
};
export default LoadingSpinner;
