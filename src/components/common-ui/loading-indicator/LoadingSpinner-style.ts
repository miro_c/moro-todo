import styled from "styled-components";


export const SpinnerImg = styled.svg`
   width: 4em;
   height: auto;
   margin-right: 1em;
`;
