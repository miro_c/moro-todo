import styled from "styled-components";
import { color, text_size }  from "../../common-ui/css_constants"


export const SnackBarMain = styled.div`
  background-color: ${color.color_black};
  position: absolute;
  text-align: center;
  width: 17em;
  height: auto;
  padding: 1em;
  margin: 0 auto;
  margin-top: -6em;
  border-radius: 0.3em;
  left: 0;
  right: 0;
  -webkit-box-shadow: 0px 0px 39px -9px rgba(0, 0, 0, 0.32);
  -moz-box-shadow: 0px 0px 39px -9px rgba(0, 0, 0, 0.32);
  box-shadow: 0px 0px 39px -9px rgba(0, 0, 0, 0.32);
  opacity: 0;
  z-index: 999;
`;

export const SnackText = styled.p`
   color: white;
   font-size: ${text_size.size_medium_mobile};
   font-weight: 400;
`;
