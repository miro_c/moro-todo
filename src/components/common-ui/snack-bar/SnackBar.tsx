import { FC, useRef, useEffect} from 'react';
import { SnackBarMain, SnackText } from './SnackBar-style';
import { gsap } from "gsap";

interface SnackBarProps {
   isOpen: boolean;
   message?: string | null;
}

const SnackBar : FC<SnackBarProps> = ({isOpen, message}: SnackBarProps) => {
  const snackRef = useRef(null);
  const TimeLine = gsap.timeline()

  useEffect(() => {
    if(isOpen){
      show();
      setTimeout(() => {
        hide();
      }, 2000);
    }

  });

  const show = () => {
    gsap.to(snackRef.current, { y: 100, opacity: 1 });
  }

  const hide = () => {
    gsap.to(snackRef.current, { y: -100, opacity: 0 });
  }

  return (
    <>
         <SnackBarMain ref={snackRef}>
         <SnackText>{message}</SnackText>
         </SnackBarMain>
    </>
  );

};
export default SnackBar;