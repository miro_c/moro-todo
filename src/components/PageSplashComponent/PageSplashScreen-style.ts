import styled, {keyframes} from "styled-components"

const fadeInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const SplashImage = styled.img`
  width: 15vw;
  height: auto;
  animation: ${fadeInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
`;