import { useHistory } from "react-router-dom";
import { useEffect } from "react";
import SplashLogo from "../../assets/svg/splash_logo.svg";
import { SplashImage } from "./PageSplashScreen-style";

const PageSplashScreen = () => {
  const history = useHistory();

  useEffect(() => {
      goToNextPage();
  });

  const goToNextPage = () => {
    setTimeout(() => {
      history.replace({
        pathname: "/todos",
      });
    }, 2000);
  }

  return (
    <>
      <SplashImage src={SplashLogo} alt="Logo" />
    </>
  );

};

export default PageSplashScreen;
