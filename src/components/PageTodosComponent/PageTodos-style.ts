import styled, {keyframes, createGlobalStyle} from "styled-components"
import { color, text_size }  from "../common-ui/css_constants"

const fadeInAnimation = keyframes`
  0% {
      opacity: 0;
      transform: scale(0.9);
  }
  100% {
      opacity: 1;
      transform: scale(1.0);
   }
`;

export const TodoMain = styled.div`
  background-color: ${color.color_background};
  display: flex;
  flex-direction: column;
  text-align: center;
  width: 80vw;
  height: 100vh;
  padding: 1em;
  animation: ${fadeInAnimation} ease 0.15s;
  animation-iteration-count: 1;
  animation-fill-mode: initial;
  z-index: 900;
`;

export const TodoTitle = styled.h1`
  color: ${color.color_material_purple};
  font-size: ${text_size.size_xxl};
  font-weight: 100;
  line-height: 0.3em;
`;

export const TodoWrapper = styled.div`
  background-color: ${color.color_white};
  padding-top: 1.3em;
  padding-left: 1.5em;
  padding-right: 1.5em;
  margin-top: 1em;
  display: flex;
  flex-direction: column;
  border: 0.05em solid ${color.color_light_grey};
  border-radius: 1em;
  -webkit-box-shadow: 0px 16px 52px 11px rgba(0,0,0,0.1);
  -moz-box-shadow: 0px 16px 52px 11px rgba(0,0,0,0.1);
  box-shadow: 0px 16px 52px 11px rgba(0,0,0,0.1);
  z-index: 900;
`;

type Props = {
  margin_top: string;
  z_index: string;
  item_width: string;
  is_visible: boolean;
};

export const TodoBottom = styled.div<Props>`
  width:  ${props => props.item_width};
  background-color: ${color.color_white};
  padding: 1.5em;
  margin: 0 auto;
  margin-top: ${props => props.margin_top};
  display: flex;
  flex-direction: column;
  border: 0.05em solid ${color.color_light_grey};
  border-radius: 1em;
  -webkit-box-shadow: 0px 16px 20px 11px rgba(0, 0, 0, 0.03);
  -moz-box-shadow: 0px 16px 20px 11px rgba(0, 0, 0, 0.03);
  box-shadow: 0px 16px 20px 11px rgba(0, 0, 0, 0.03);
  z-index: ${props => props.z_index};
  display: ${props => props.is_visible? "visible" : "none"};
`;


/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const TodoResponsiveStyle = createGlobalStyle`


 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${TodoMain} {
       width: 90vw;
    }
    ${TodoTitle} {
      font-size: 3rem;
    }
  }

  @media all and (min-width: 480px) and (max-width: 568px) {
    ${TodoTitle} {
      font-size: 3.2rem;
    }
    ${TodoMain} {
      width: 80vw;
    }
  }

  @media all and (min-width: 568px) and (max-width: 768px) {
    ${TodoTitle} {
      font-size: 3.5rem;
    }
    ${TodoMain} {
      width: 70vw;
    }
  }

  @media screen and (min-width: 768px) and (max-width: 1024px) {
    ${TodoMain} {
      width: 60vw;
    }
  }

  @media screen and (min-width: 1024px) and (max-width: 1920px) {
    ${TodoMain} {
      width: 35vw;
    }
  }
`;



