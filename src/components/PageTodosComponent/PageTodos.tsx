import { useEffect, useState, useCallback } from "react";
import {
  TodoMain,
  TodoTitle,
  TodoResponsiveStyle,
  TodoWrapper,
  TodoBottom,
} from "./PageTodos-style";
import TodoInput from "./todo-components/todo-input/TodoInput";
import TodoInfoBar from "./todo-components/todo-info-bar/TodoInfoBar";
import TodoList from "./todo-components/TodoList";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../redux-store/AppState";
import SnackBar from "../../components/common-ui/snack-bar/SnackBar";
import { DispatchTodoState, DispatchAppStateToDefaultAction, actionDispatchHttpError } from "../../redux-store/DispatcherActions";
import { AxiosResponse } from "axios";
import { HttpActionGetTodosAndDispatchState } from "../../api/TodoHttpActions";
import { TodoReducerActionEnum } from "../../redux-store/TodoReducer";
import { AppStateObjectReducerEnum } from "../../redux-store/AppStateObjectReducer";
import { Todo } from "../../model/Todo";


const PageTodos = () => {
  const dispatch = useDispatch();
  const [isOpenSnack, setOpenSnack] = useState<boolean>(false);
  const todoUser = useSelector((state: AppState) => state.todo_user);
  const appStateObject = useSelector(
    (state: AppState) => state.app_state_object
  );

  /***************************************************
   *      --- Init Http Request GetTodos ---
   * *************************************************/
  useEffect(() => {
    // --- Call Http Get todos request and dispach response state succes/error ---
    const asyncCall = async () => {
      await HttpActionGetTodosAndDispatchState(
        dispatch,
        actionResponseSucces,
        actionResponseError
      );
    };
    asyncCall();
  }, []);

  // --- Show/Hide Snack Bar from any Components ---
  useEffect(() => {
    if (appStateObject) {
      if (appStateObject?.isError) {
        setOpenSnack(true);
      }
    }
  }, [appStateObject, appStateObject?.isError]);

  /***************************************************
   *       --- Dispatch response methods ---
   * *************************************************/
  const actionShowMessage = useCallback(() => {
    setOpenSnack(true);
    setTimeout(() => {
      actionHideMessage();
    }, 3000);
  }, []);

  const actionResponseSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      let todoList = response.data as Array<Todo>;
      if (todoUser) {
        todoUser.todos = todoList;
        DispatchTodoState(TodoReducerActionEnum.ACTION_GET_TODOS, dispatch, todoUser);
      } else {
        DispatchAppStateToDefaultAction(dispatch)
      }
    },
    [todoUser, dispatch]
  );

  const actionResponseError = useCallback(
    (error: any) => {
      actionDispatchHttpError(AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT, dispatch, error, appStateObject);
      actionShowMessage();
    },
    [appStateObject, dispatch, actionShowMessage]
  );

  const actionHideMessage = () => {
    setOpenSnack(false);
  };

  return (
    <>
      <TodoResponsiveStyle />

      <TodoMain>
        {appStateObject?.isError && (
          <SnackBar
            isOpen={isOpenSnack}
            message={appStateObject?.errorMessage}
          />
        )}

        <TodoTitle>Moro todos</TodoTitle>
        <TodoWrapper>
          <TodoInput />
          {todoUser && <TodoList todoUser={todoUser} />}
          {todoUser?.todos && todoUser?.todos.length > 0 && (
            <TodoInfoBar todoUser={todoUser} />
          )}
        </TodoWrapper>

        {/* Only for more todos list bottom effect */}
        {todoUser?.todos && (
          <TodoBottom
            is_visible={todoUser?.todos?.length > 1}
            margin_top={"-2.3em"}
            z_index={"-9"}
            item_width={"95%"}
          />
        )}
        {todoUser?.todos && (
          <TodoBottom
            is_visible={todoUser?.todos?.length > 1}
            margin_top={"-2.3em"}
            z_index={"-10"}
            item_width={"90%"}
          />
        )}
      </TodoMain>
    </>
  );
};

export default PageTodos;
