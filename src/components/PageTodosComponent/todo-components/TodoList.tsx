import { FC } from "react";
import TodoCreated from "./todo-created/TodoCreated";
import { TodoUser } from "../../../model/TodoUser";
import { useSelector } from "react-redux";
import { AppState } from "../../../redux-store/AppState";
import { TodoActionState } from "../../../model/TodoState";


interface TodoListProps {
    todoUser: TodoUser | null;
}

const TodoList: FC<TodoListProps> = ({todoUser}: TodoListProps) => {
  const todoState = useSelector((state: AppState) => state.todo_state);

  return (
    <>
      {/* Show All todos */}
      {todoState && todoState.todo_state === TodoActionState.ACTION_ALL &&
        todoUser?.todos?.map((todo) => (
          <TodoCreated key={todo.id} todoItem={todo} />
        ))}

      {/* Show Active todos */}
      {todoState && todoState.todo_state === TodoActionState.ACTION_ACTIVE &&
        todoUser?.todos?.map((todo) =>
          !todo.completed ? <TodoCreated key={todo.id} todoItem={todo} /> : null
        )}

      {/* Show Completed todos */}
      {todoState && todoState.todo_state === TodoActionState.ACTION_COMPLETED &&
        todoUser?.todos?.map((todo) =>
          todo.completed ? <TodoCreated key={todo.id} todoItem={todo} /> : null
        )}
    </>
  );

};

export default TodoList;