import styled, {createGlobalStyle} from "styled-components"
import { color, text_size }  from "../../../common-ui/css_constants"


export const TodoCreatedWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 4em;
  padding-top: 0.5em;
  padding-bottom: 0.5em;
  border-top: 0.1em solid ${color.color_light_grey};
  cursor: pointer;
`;

export const IconDoneWrapper = styled.div`
  width: 3em;
  height: auto;
  cursor: pointer;
`;

export const IconDone = styled.img`
  width: 3em;
  height: auto;
  padding: 0.5em;
  cursor: pointer;
`;

export const TodoInputMain = styled.input`
  margin-left: 0.5em;
  width: 100%;
  height: auto;
  font-size: ${text_size.size_medium};
  font-weight: 400;
  border: 0.1em solid ${color.color_material_purple};
  color: ${color.color_dark_grey};
  border-radius: 0.3em;
  padding: 0.2em;

  :focus {
    outline: none;
  }
  ::placeholder {
    margin-left: 0.5em;
    color: ${color.color_dark_grey};
    font-size: ${text_size.size_medium};
    font-family: "Roboto", sans-serif;
    font-style: normal;
  }
`;

type Props = {
  text_decoration: boolean;
};
export const TodoText = styled.p<Props>`
  margin-top: 0.30em;
  margin-left: 0.5em;
  font-size:  ${text_size.size_medium};
  font-weight: 400;
  border: 0;
  text-decoration-line: ${props => props.text_decoration? 'line-through' : 'none'};
  color: ${props => props.text_decoration? color.color_material_purple : color.color_dark_grey};
`;

export const IconDelete = styled.img`
  width: 2em;
  height: auto;
  padding: 0.5em;
  cursor: pointer;
  margin-left: auto;
  margin-right: 0.8em;
  transition: width 0.3s, height 0.3s, transform 0.3s;
  :hover {
    width: 2.3em;
    transform: rotate(90deg);
  }
`;


/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const TodoCreatedResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
     ${TodoCreatedWrapper}{
       height: 3em;
       padding-top: 0.15em;
       padding-bottom: 0.15em;
     }
    ${TodoInputMain}{
      font-size: ${text_size.size_medium_mobile};
    }
     ${IconDoneWrapper}{
       width: 2em;
     }

     ${IconDone}{
       width: 2em;
       padding: 0.3em;
      }

      ${IconDelete}{
        width: 1.7em;
        padding: 0.5em;
        margin-right: 0.6em;
      }

      ${TodoText}{
        margin-top: 0.15em;
        margin-left: 0.2em;
        font-size:  ${text_size.size_medium_mobile};
      }
  }

  @media all and (min-width: 480px) and (max-width: 568px) {
    ${TodoCreatedWrapper}{
       height: 3em;
       padding-top: 0.5em;
       padding-bottom: 0.1em;
     }
      ${TodoInputMain}{
        font-size: ${text_size.size_medium_mobile};
      }
      ${IconDoneWrapper}{
        width: 2em;
     }

     ${IconDone}{
       width: 2em;
       padding: 0.3em;
      }

      ${IconDelete}{
        width: 1.8em;
        padding: 0.5em;
        margin-right: 0.6em;
      }
      ${TodoText}{
        margin-top: 0.15em;
        margin-left: 0.2em;
        font-size:  ${text_size.size_medium_mobile};
      }
  }

  @media all and (min-width: 568px) and (max-width: 768px) {

  }

  @media screen and (min-width: 768px) and (max-width: 1024px) {

  }

  @media screen and (min-width: 1024px) and (max-width: 1920px) {

  }
`;