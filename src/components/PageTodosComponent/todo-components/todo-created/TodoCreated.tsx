import { useEffect, FC, useCallback, useState } from "react";
import {
  TodoCreatedWrapper,
  IconDoneWrapper,
  IconDone,
  TodoText,
  IconDelete,
  TodoInputMain,
  TodoCreatedResponsiveStyle
} from "./TodoCreated-style";
import ic_done from "../../../../assets/svg/ic_done.svg";
import ic_delete from "../../../../assets/svg/ic_delete.svg";
import { Todo } from "../../../../model/Todo";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../../../redux-store/AppState";
import { AxiosResponse } from "axios";
import {
  DispatchTodoState,
  actionDispatchHttpError
 
} from "../../../../redux-store/DispatcherActions";
import {
  HttpActionDeleteTodoAndDispatchState,
  HttpActionCompleteTodoAndDispatchState,
  HttpActionInCompleteTodoAndDispatchState,
  HttpActionUpdateTodoAndDispatchState
} from "../../../../api/TodoHttpActions";
import { TodoReducerActionEnum } from "../../../../redux-store/TodoReducer";
import {
  getFilteredTodosByTodoId,
  getMarkCompletedToggleTodos,
} from "../../../../utils/DataFilterUtil";
import { AppStateObjectReducerEnum } from "../../../../redux-store/AppStateObjectReducer";


interface TodoCreatedProps {
  todoItem: Todo;
}

const TodoCreated: FC<TodoCreatedProps> = ({ todoItem }: TodoCreatedProps) => {
  const [isEditState, setEditState] = useState<boolean>(false);
  const [textInputValue, setTextInputValue] = useState<string>();
  const todoUser = useSelector((state: AppState) => state.todo_user);
  const appStateObject = useSelector(
    (state: AppState) => state.app_state_object
  );
  const dispatch = useDispatch();

  useEffect(() => {});

  const todoActionToggleDone = () => {
    if (!isEditState) {
      if (!todoItem.completed) {
        // --- Call Http Post Complete todo request and dispach response state succes/error ---
        const asyncCallComplete = async () => {
          await HttpActionCompleteTodoAndDispatchState(
            dispatch,
            todoItem.id,
            actionResponseInCompleteSucces,
            actionResponseError
          );
        };
        asyncCallComplete();

      } else {
        // --- Call Http Post InComplete todo request and dispach response state succes/error ---
        const asyncCallInComplete = async () => {
          await HttpActionInCompleteTodoAndDispatchState(
            dispatch,
            todoItem.id,
            actionResponseCompleteSucces,
            actionResponseError
          );
        };
        asyncCallInComplete();
      }
    } else {
      setEditState(false);
    }
  };

  const todoActionDelete = () => {
    // --- Call Http Post Delete todo request and dispach response state succes/error ---
    const asyncCallDelete = async () => {
      await HttpActionDeleteTodoAndDispatchState(
        dispatch,
        todoItem.id,
        actionResponseDeleteSucces,
        actionResponseError
      );
    };
    asyncCallDelete();
  };

  /***************************************************
   *      --- Dispatch response methods ---
   * *************************************************/
  const actionResponseDeleteSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      if (response.status === 200) {
        if (todoUser) {
          const filteredTodos = getFilteredTodosByTodoId(todoUser, todoItem.id);
          todoUser.todos = filteredTodos;
          DispatchTodoState(TodoReducerActionEnum.ACTION_DELETE_TODO, dispatch, todoUser);
        }
      }
    },
    [dispatch, todoItem.id, todoUser]
  );

  const actionResponseCompleteSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      if (response.status === 200) {
        if (todoUser != null) {
          const filteredTodos = getMarkCompletedToggleTodos(todoUser, todoItem.id);
          todoUser.todos = filteredTodos;
          DispatchTodoState(TodoReducerActionEnum.ACTION_COMPLETE_TODO, dispatch, todoUser);
        }
      }
    },
    [dispatch, todoItem.id, todoUser]
  );

  const actionResponseInCompleteSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      if (response.status === 200) {
        if (todoUser != null) {
          const filteredTodos = getMarkCompletedToggleTodos(todoUser, todoItem.id);
          todoUser.todos = filteredTodos;
          DispatchTodoState(TodoReducerActionEnum.ACTION_INCOMPLETE_TODO, dispatch, todoUser);
        }
      }
    },
    [dispatch, todoItem.id, todoUser]
  );

  const actionResponseUpdateSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      if (response.status === 200) {
        const updatedTodo = response.data as Todo;
        if (updatedTodo) {
          todoUser?.todos?.map((todo) => {
            if (todo.id === updatedTodo.id) {
              todo.text = updatedTodo.text;
            }
            return todo;
          });
        }
        if (todoUser) {
          DispatchTodoState(TodoReducerActionEnum.ACTION_CREATE_TODO, dispatch, todoUser);
        }
        setEditState(false);
      }
    },
    [dispatch, todoUser]
  );

  // --- Http error response method (for all requests) ---
  const actionResponseError = useCallback(
    (error: any) => {
      actionDispatchHttpError(AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT, dispatch, error, appStateObject);
    },
    [appStateObject, dispatch]
  );

  const actionDoubleClickEditTodo = () => {
    setEditState(true);
  };

  const actionClickEditTodo = () => {
    setEditState(false)
    
  };

  // --- Text input Action methods ---
  const onChangeTextInput = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const todoTextInput = e.target.value ? e.target.value : "";
    setTextInputValue(todoTextInput);
  };

  const onPressEnterTextInput = async (
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (e.code === "Enter" || e.code === "NumpadEnter") {
      const todoTextInput = e.currentTarget.value;

      // --- Call Http Post create todo request and dispach response state succes/error ---
      const asyncUpdateCall = async () => {
        await HttpActionUpdateTodoAndDispatchState(
          dispatch,
          todoItem.id,
          todoTextInput,
          actionResponseUpdateSucces,
          actionResponseError
        );
      };
      asyncUpdateCall();
    }
  };

  return (
    <>
      <TodoCreatedResponsiveStyle/>
      <TodoCreatedWrapper onClick={actionClickEditTodo} onDoubleClick={actionDoubleClickEditTodo}>
        <IconDoneWrapper onClick={todoActionToggleDone}>
          {!isEditState && todoItem.completed && <IconDone src={ic_done} />}
        </IconDoneWrapper>

        {!isEditState ? (
          <TodoText text_decoration={todoItem.completed}>
            {todoItem.text}
          </TodoText>
        ) : (
          <TodoInputMain
            type="text"
            placeholder={todoItem.text}
            onChange={onChangeTextInput}
            onKeyPress={onPressEnterTextInput}
            value={textInputValue || textInputValue?.length === 0? textInputValue : todoItem.text}
            autoFocus={true}
          ></TodoInputMain>
        )}

        {!isEditState && (
          <IconDelete src={ic_delete} onClick={todoActionDelete} />
        )}
      </TodoCreatedWrapper>
    </>
  );
};

export default TodoCreated;
