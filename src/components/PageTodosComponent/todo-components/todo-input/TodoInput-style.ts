import styled, {keyframes, createGlobalStyle} from "styled-components"
import { color, text_size }  from "../../../common-ui/css_constants"

export const TodoInputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  text-align: center;
  margin-bottom:1.5em;
`;

export const IconDownWrapper = styled.div`
  width: 3em;
  height: auto;
  cursor: pointer;
`;

export const IconDown = styled.img`
  width: 2.8em;
  height: auto;
  padding: 0.5em;
  cursor: pointer;
`;

export const TodoInputMain = styled.input`
  margin-left: 0.5em;
  width: 100%;
  height: auto;
  font-size: ${text_size.size_medium};
  font-weight: 400;
  border: 0;
  color: ${color.color_dark_grey};

  :focus {
    outline: none;
  }
`;


/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const TodoInputResponsiveStyle = createGlobalStyle`

::placeholder {
  margin-left: 0.5em;
  color: ${color.color_light_grey_italic};
  font-size: ${text_size.size_medium};
  font-family: 'Roboto', sans-serif;
  font-style: italic;
}

 /*******  Mobile **********/
 @media all and (min-width: 320px) and (max-width: 480px) {
    ${TodoInputWrapper}{
      margin-bottom:0.5em;
    }
    ${TodoInputMain} {
      font-size: ${text_size.size_medium_mobile};
    }
    ::placeholder {
       margin-left: 0.3em;
       font-size: ${text_size.size_medium_mobile};
    }

    ${IconDownWrapper}{
      width: 2em;
    }

    ${IconDown}{
      width: 2em;
      padding: 0.3em;
    }

  }

  @media all and (min-width: 480px) and (max-width: 568px) {
    ${TodoInputWrapper}{
      margin-bottom:0.5em;
    }
    ${TodoInputMain} {
      font-size: ${text_size.size_medium_mobile};
    }
    ::placeholder {
       margin-left: 0.3em;
       font-size: ${text_size.size_medium_mobile};
    }

    ${IconDownWrapper}{
      width: 2em;
    }

    ${IconDown}{
      width: 2em;
      padding: 0.3em;
    }

  }

  @media all and (min-width: 568px) and (max-width: 768px) {
    ${TodoInputWrapper}{
      margin-bottom:0.7em;
    }
  }

  @media screen and (min-width: 768px) and (max-width: 1024px) {

  }

  @media screen and (min-width: 1024px) and (max-width: 1920px) {
    ${TodoInputWrapper}{
      margin-bottom:0.8em;
    }
  }
`;