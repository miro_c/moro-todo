import { useState, useCallback } from "react";
import {
  IconDownWrapper,
  IconDown,
  TodoInputMain,
  TodoInputResponsiveStyle,
  TodoInputWrapper,
} from "./TodoInput-style";
import ic_narrow_down_default from "../../../../assets/svg/narrow_down_default.svg";
import ic_narrow_down_active from "../../../../assets/svg/narrow_down_active.svg";
import LoadingSpinner from "../../../../components/common-ui/loading-indicator/LoadingSpinner";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../../../redux-store/AppState";
import { DispatchTodoState, actionDispatchHttpError } from "../../../../redux-store/DispatcherActions";
import { HttpActionCreateNewTodoAndDispatchState } from "../../../../api/TodoHttpActions";
import { AxiosResponse } from "axios";
import { TodoReducerActionEnum } from "../../../../redux-store/TodoReducer";
import { Todo } from "../../../../model/Todo";
import { getMarkCompletedFilteredTodos } from "../../../../utils/DataFilterUtil";
import { AppStateObjectReducerEnum } from "../../../../redux-store/AppStateObjectReducer";


const TodoInput = () => {
  const dispatch = useDispatch();
  const todoUser = useSelector((state: AppState) => state.todo_user);
  const [textInputValue, setTextInputValue] = useState<string>();
  const appStateObject = useSelector((state: AppState) => state.app_state_object);

  // --- Text input Action methods ---
  const onChangeTextInput = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const todoTextInput = e.target.value ? e.target.value : "";
    setTextInputValue(todoTextInput);
  };

  const onPressEnterTextInput = async (
    e: React.KeyboardEvent<HTMLInputElement>
  ) => {
    if (e.code === "Enter" || e.code === "NumpadEnter") {
      const todoTextInput = e.currentTarget.value;

      // --- Call Http Post create todo request and dispach response state succes/error ---
      const asyncCall = async () => {
        await HttpActionCreateNewTodoAndDispatchState(
          dispatch,
          todoTextInput,
          actionResponseSucces,
          actionResponseError
        );
      };
      asyncCall();
    }
  };

  /***************************************************
   *      --- Dispatch response methods ---
   * *************************************************/
   const actionResponseSucces = useCallback(
    (response: AxiosResponse<unknown, any>) => {
      if (todoUser) {
        todoUser.todos?.push(response.data as Todo);
        DispatchTodoState(TodoReducerActionEnum.ACTION_CREATE_TODO, dispatch, todoUser);
        clearTextInput();
      }
    },
    [todoUser, dispatch]
  );

  const actionResponseError = useCallback(
    (error: any) => {
      actionDispatchHttpError(AppStateObjectReducerEnum.ACTION_APP_STATE_OBJECT, dispatch, error, appStateObject);
    },
    [appStateObject, dispatch]
  );

  const clearTextInput = () => {
    setTextInputValue("");
  }
  
  const actionTodoAllMarkDone = () => {
    if (todoUser) {
      const filteredTodos = getMarkCompletedFilteredTodos(todoUser);
      todoUser.todos = filteredTodos;
      DispatchTodoState(TodoReducerActionEnum.ACTION_GET_COMPLETED_TODOS, dispatch, todoUser);
    }
  };

  return (
    <>
      <TodoInputResponsiveStyle />
      <TodoInputWrapper>
        <IconDownWrapper>
          {todoUser?.todos && todoUser?.todos.length > 0 ? (
            <IconDown
              src={ic_narrow_down_active}
              onClick={actionTodoAllMarkDone}
            />
          ) : (
            <IconDown
              src={ic_narrow_down_default}
              onClick={actionTodoAllMarkDone}
            />
          )}
        </IconDownWrapper>

        <TodoInputMain
          type="text"
          placeholder="What needs to be done?"
          onChange={onChangeTextInput}
          onKeyPress={onPressEnterTextInput}
          value={textInputValue ? textInputValue : ""}
          autoFocus={true}
        ></TodoInputMain>

        {appStateObject?.isLoading && <LoadingSpinner />}
      </TodoInputWrapper>
    </>
  );
};

export default TodoInput;
