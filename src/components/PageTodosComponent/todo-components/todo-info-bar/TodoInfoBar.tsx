import { FC } from "react";
import {
  TodoInfoBarWrapper,
  TodoInfoTextWrapper,
  TodoInfoCenter,
  TodoInfoCenterText,
  TodoInfoText,
  TodoInfoActionText,
  TodoInfoTextSpan,
  TodoInfoBarResponsiveStyle
} from "./TodoInfoBar-style";
import { TodoUser } from "../../../../model/TodoUser";
import { dispatchTodoActionState } from "../../../../redux-store/DispatcherActions";
import { TodoActionState } from "../../../../model/TodoState";
import { useDispatch, useSelector } from "react-redux";
import { AppState } from "../../../../redux-store/AppState";
import { TodoStateEnum } from "../../../../redux-store/TodoStateReducer";

interface TodoInfoBarProps {
  todoUser: TodoUser | null;
}

const TodoInfoBar: FC<TodoInfoBarProps> = ({ todoUser }: TodoInfoBarProps) => {
  const todoState = useSelector((state: AppState) => state.todo_state);
  const dispatch = useDispatch();

  const actionTodosAll = () => {
    dispatchTodoActionState(
      dispatch,
      todoState,
      TodoActionState.ACTION_ALL,
      TodoStateEnum.ACTION_TODO_STATE
    );
  };

  const actionTodosActive = () => {
    dispatchTodoActionState(
      dispatch,
      todoState,
      TodoActionState.ACTION_ACTIVE,
      TodoStateEnum.ACTION_TODO_STATE
    );
  };

  const actionTodosCompleted = () => {
    dispatchTodoActionState(
      dispatch,
      todoState,
      TodoActionState.ACTION_COMPLETED,
      TodoStateEnum.ACTION_TODO_STATE
    );
  };

  return (
    <>
    <TodoInfoBarResponsiveStyle/>
      <TodoInfoBarWrapper>
        <TodoInfoTextWrapper>
          <TodoInfoText><TodoInfoTextSpan>{todoUser?.todos?.length}</TodoInfoTextSpan> items </TodoInfoText>
        </TodoInfoTextWrapper>

        <TodoInfoCenter>
          <TodoInfoCenterText is_active={todoState?.todo_state === TodoActionState.ACTION_ALL} onClick={actionTodosAll}>All</TodoInfoCenterText>
          <TodoInfoCenterText is_active={todoState?.todo_state === TodoActionState.ACTION_ACTIVE} onClick={actionTodosActive}>Active</TodoInfoCenterText>
          <TodoInfoCenterText is_active={todoState?.todo_state === TodoActionState.ACTION_COMPLETED} onClick={actionTodosCompleted}>Completed</TodoInfoCenterText>
        </TodoInfoCenter>

        <TodoInfoTextWrapper>
          <TodoInfoActionText>Clear completed</TodoInfoActionText>
        </TodoInfoTextWrapper>
      </TodoInfoBarWrapper>
    </>
  );
};

export default TodoInfoBar;
