import styled, {keyframes, createGlobalStyle} from "styled-components"
import { color, text_size }  from "../../../common-ui/css_constants"


export const TodoInfoBarWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: auto;
  padding-top: 1.5em;
  padding-bottom: 0.7em;
  border-top: 0.1em solid ${color.color_light_grey};
`;

export const TodoInfoTextWrapper = styled.div`
   padding: 0.5em;
   width: 30vw;
   height: auto;
`;
export const TodoInfoText = styled.p`
  font-size: ${text_size.size_medium_mobile};
  font-weight: 400;
  border: 0;
  color: ${color.color_grey};
`;
export const TodoInfoTextSpan = styled.span`
  font-size: ${text_size.size_medium_mobile};
  font-weight: 600;
  color: ${color.color_purple};
  margin-right: 0.3em;
  margin-left: -1.2em;
`;

export const TodoInfoCenter = styled.div`
  width: 70vw;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  text-align: center;
`;

export const TodoInfoActionText = styled(TodoInfoText)`
  :hover {
    color: ${color.color_dark_grey};
  }
  cursor: pointer;
  -webkit-transition: color 0.25s;
  transition: color 0.25s;
  cursor: pointer;
`;


type InfoTextProps = {
  is_active: boolean;
};
export const TodoInfoCenterText = styled.p<InfoTextProps>`
  font-size: ${text_size.size_medium_mobile};
  font-weight: 400;
  border: 0;
  color: ${props => props.is_active? color.color_purple : color.color_grey};
  text-decoration: ${props => props.is_active? 'underline' : 'none'};
  padding-right: 1em;

  :hover {
    color: ${color.color_purple};
  }

  :active {
    text-decoration: underline;
  }

  cursor: pointer;
  -webkit-transition: color 0.25s;
  transition: color 0.25s;
`;

/**********************************************************************
 * 
 *                 --- Responsive Style ---
 * 
 **********************************************************************/
 export const TodoInfoBarResponsiveStyle = createGlobalStyle`

 /*******  Mobile **********/
  @media all and (min-width: 320px) and (max-width: 480px) {
    ${TodoInfoText}{
      font-size: ${text_size.size_small};
    }

    ${TodoInfoCenterText}{
      font-size: ${text_size.size_small};
    }

    ${TodoInfoTextSpan}{
      font-size: ${text_size.size_small};
    }
    
  }

  @media all and (min-width: 480px) and (max-width: 568px) {
    ${TodoInfoText}{
      font-size: ${text_size.size_small};
    }

    ${TodoInfoCenterText}{
      font-size: ${text_size.size_small};
    }

    ${TodoInfoTextSpan}{
      font-size: ${text_size.size_small};
    }

  }

  @media all and (min-width: 568px) and (max-width: 768px) {
    ${TodoInfoText}{
      font-size: ${text_size.size_medium_info};
    }

    ${TodoInfoCenterText}{
      font-size: ${text_size.size_medium_info};
    }

    ${TodoInfoTextSpan}{
      font-size: ${text_size.size_medium_info};
    }
  }

  @media screen and (min-width: 768px) and (max-width: 1024px) {
    ${TodoInfoText}{
      font-size: ${text_size.size_medium_mobile};
    }

    ${TodoInfoCenterText}{
      font-size: ${text_size.size_medium_mobile};
    }

    ${TodoInfoTextSpan}{
      font-size: ${text_size.size_medium_mobile};
    }

  }

  @media screen and (min-width: 1024px) and (max-width: 1920px) {
    ${TodoInfoText}{
      font-size: ${text_size.size_medium_mobile};
    }

    ${TodoInfoCenterText}{
      font-size: ${text_size.size_medium_mobile};
    }

    ${TodoInfoTextSpan}{
      font-size: ${text_size.size_medium_mobile};
    }
  }
`;