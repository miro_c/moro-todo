import { FC } from 'react';
import { ErrorImg, ErrorTitle } from './PageError-style';
import errorLogo from "../../assets/svg/error_logo.svg";

const PageError : FC = () => {

  return (
    <>
      <ErrorImg src={errorLogo} alt="Error" />
      <ErrorTitle>404 error</ErrorTitle>
    </>
  );

};
export default PageError;