import styled, { css } from "styled-components";
import { color, text_size }  from "../common-ui/css_constants"

export const ErrorImg = styled.img`
  width: 15vw;
  height: auto;
`;

export const ErrorTitle = styled.p`
  color: #7a7a7a;
  font-size: 3rem;
  font-weight: 600;

  /* Mobile screen */
  @media (min-width: 320px) and (max-width: 480px) {
    font-size: 1.5rem;
    font-weight: 500;
  }
`;